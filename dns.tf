variable "route53_domain" {
  type = string
}

variable "server_name" {
  type    = string
  default = "ant"
}

data "aws_route53_zone" "selected" {
  name         = var.route53_domain
  private_zone = false
}

resource "aws_route53_record" "server" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "${var.server_name}.${data.aws_route53_zone.selected.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_spot_instance_request.server.0.public_ip]
}