# Terraform Script to Install Ant Media Server CE from AMI

Terraform script to spin up latest Ant Media Server CE from published AMI on a Spot Instance in AWS.

1. Install terraform from https://www.terraform.io/downloads.html
1. Update `variable "ssh_key_name"` with your ssh key name from the correct region or define it in a [terraform.tfvars](https://www.terraform.io/docs/configuration/variables.html#variable-definitions-tfvars-files) file.
1. Ensure your [AWS access-keys are accessable to terraform](https://www.terraform.io/docs/providers/aws/index.html#environment-variables).


```sh
terraform init
terraform plan
terraform apply
```

You will get an output of the public IP address of the new server, it will take a few minutes to start up. Access via `http://public_ip:5080`
