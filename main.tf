variable "ssh_key_name" { description = "Name of AWS ssh key create ahead of time." }
variable "aws_region_name" { description = "AWS region name i.e. us-west-2" }
variable "instance_type" { default = "c3.large" }
variable "ce-or-ee" { default = "CE" }

provider "aws" {
  # Use keys in home dir.
  #  access_key = "ACCESS_KEY_HERE"
  #  secret_key = "SECRET_KEY_HERE"
  region = var.aws_region_name
}

data "aws_ami" "antmedia" {
  most_recent = true

  filter {
    name   = "name"
    values = ["AntMedia-AWS-Marketplace-${var.ce-or-ee}*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  owners = ["679593333241"] # AntMedia
}

# Request a spot instance at $1.00
# Master Node
resource "aws_spot_instance_request" "server" {
  wait_for_fulfillment = true
  count                = "1"
  ami                  = data.aws_ami.antmedia.id
  spot_price           = "1.00"
  instance_type        = var.instance_type
  spot_type            = "one-time"

  vpc_security_group_ids = [aws_security_group.antmedia_sg.id]

  key_name = var.ssh_key_name

  user_data = <<EOT
#!/bin/bash
cd /usr/local/antmedia
./enable_ssl.sh -d ${var.server_name}.${data.aws_route53_zone.selected.name}
EOT
  tags = {
    Name = "Ant Media CE"
    App  = "AntMedia"
  }
}

output "master_ip" {
  value = aws_spot_instance_request.server.0.public_ip
}

output "instance_id" {
  value = aws_spot_instance_request.server.0.spot_instance_id
}

output "root_login" {
  value = "Server: http://${aws_spot_instance_request.server.0.public_ip}:5080\nUsername: JamesBond\nPassword: ${aws_spot_instance_request.server.0.spot_instance_id}"
}

output "secure_web_login" {
  value = "Server: https://${var.server_name}.${data.aws_route53_zone.selected.name}:5443\nUsername: JamesBond\nPassword: ${aws_spot_instance_request.server.0.spot_instance_id}"
}