resource "aws_security_group" "antmedia_sg" {
  description = "Ports for Ant Media"
}

resource "aws_security_group_rule" "sg-antmedia-out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Any Egress"

  security_group_id = aws_security_group.antmedia_sg.id
}


resource "aws_security_group_rule" "sg-antmedia-5000-6500" {
  type        = "ingress"
  from_port   = 5000
  to_port     = 6500
  protocol    = "udp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "Media Ports for Streaming"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-22" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "ssh"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-80" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "www"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-5080" {
  type        = "ingress"
  from_port   = 5080
  to_port     = 5080
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "www"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-1935" {
  type        = "ingress"
  from_port   = 1935
  to_port     = 1935
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "rtmp"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-5554" {
  type        = "ingress"
  from_port   = 5554
  to_port     = 5554
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = ""

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-5443" {
  type        = "ingress"
  from_port   = 5443
  to_port     = 5443
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  description = "https"

  security_group_id = aws_security_group.antmedia_sg.id
}

resource "aws_security_group_rule" "sg-antmedia-4200" {
  type        = "ingress"
  from_port   = 4200
  to_port     = 4200
  protocol    = "udp"
  cidr_blocks = ["0.0.0.0/0"]
  description = ""

  security_group_id = aws_security_group.antmedia_sg.id
}